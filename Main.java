import java.util.Scanner;

public class Main{
    public static String calc(String input){
        String result;
        int sum ;
        Converter converter = new Converter();
        String[] math_operation_array = {"+", "-", "/", "*"};
        String[] regex_array = {"\\+", "-", "/", "\\*"};
        //Определяем арифметическое действие:

        int arrIndex=-1;
        for(int i = 0; i < math_operation_array.length; i++) {
            if(input.contains(math_operation_array[i])){
                arrIndex = i;
                break;
            }
        }

        if(arrIndex == -1){
                try {
                    throw new Exception();
                } catch (Exception e) {
                    return "throws Exception //т.к. строка не является математической операцией";
                }
        }
        //Делим строчку по найденному арифметическому знаку


        String[] data = input.split(regex_array[arrIndex]);
        if(data.length <= 2){
            //Определяем, находятся ли числа в одном формате (оба римские или оба арабские)
            if(converter.isRoman(data[0]) == converter.isRoman(data[1])){
                int num_1,num_2;
                //Определяем, римские ли это числа
                boolean isRoman = converter.isRoman(data[0]);
                if(isRoman){
                    //если римские, то конвертируем их в арабские

                    num_1 = converter.romanToInt(data[0]);
                    num_2 = converter.romanToInt(data[1]);

                }
                else{
                    //если арабские, конвертируем их из строки в число
                    num_1 = Integer.parseInt(data[0]);
                    num_2 = Integer.parseInt(data[1]);
                }
                if((num_1 > 0 && num_1 < 11 ) && (num_2 > 0 && num_2 < 11) ){

                    //выполняем с числами арифметическое действие
                    switch (math_operation_array[arrIndex]){
                        case "+":
                            sum = num_1+num_2;
                            break;
                        case "-":
                            sum = num_1-num_2;
                            break;
                        case "*":
                            sum = num_1*num_2;
                            break;
                        default:
                            sum = num_1/num_2;
                            break;
                    }
                }
                else{
                    try {
                        throw new Exception();
                    }catch (Exception e){
                        return  "Вы привысили диапозон вводимых значений от 1 до 10.";
                    }
                }

                //15->XV
                if(isRoman){
                    //если числа были римские, возвращаем результат в римском числе
                    if(sum < 0){
                        try{
                            throw new Exception();
                        }catch (Exception e){
                            result = "throws Exception //т.к. в римской системе нет отрицательных чисел.";
                        }
                    }
                    result = converter.intToRoman(sum);
                }
                else{
                    //если числа были арабские, возвращаем результат в арабском числе
                    result = Integer.toString(sum);
                }
            }
            else{
                try{
                    throw new Exception();
                }catch(Exception e){
                    result = "throws Exception //т.к. используются одновременно разные системы счисления.";
                }
            }
        }
        else{
            try{
                throw new Exception();
            }catch (Exception e){
                result = "throws Exception //т.к. формат математической операции не удовлетворяет заданию - два операнда и один оператор.";

            }
        }

        return result;
    }
    public static void main(String[] args) {

        Scanner scn = new Scanner(System.in);
        System.out.print("Введите выражение: ");
        String exp = scn.nextLine().toUpperCase();
        System.out.printf("Ответ: " + calc(exp));


    }
}